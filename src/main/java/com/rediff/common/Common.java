package com.rediff.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeSuite;

public class Common {
	public WebDriver driver = null;
	public Logger log = null;
	public Properties env = null;
	String path = System.getProperty("user.dir");

	@BeforeSuite
	public void init() throws IOException {
		log = Logger.getLogger("rootLogger");
		log.debug("Logger is initlalized successfully");

		InputStream fis = new FileInputStream(
				"/Users/mkanumukkala/Documents/manidhar/git/B35/Computer2/b35/src/main/resources/Environment.properties");
		env = new Properties();
		env.load(fis);
		log.debug("Environment properties file loaded successfully");
	}

	public void openBrowser() {
		
		String browserName=env.getProperty("browser");
		
		if(browserName.equalsIgnoreCase("chrome")) {
		
		System.setProperty("webdriver.chrome.driver", path + "/src/main/resources/drivers/mac/chrome/chromedriver");
		driver = new ChromeDriver(); 
		}else if(browserName.equalsIgnoreCase("firefox"))
		{
			System.setProperty("webdriver.gecko.driver", path + "/src/main/resources/drivers/mac/firefox/geckodriver");
			 driver=new FirefoxDriver();
		}else if(browserName.equalsIgnoreCase("safari"))
		{
			 driver=new SafariDriver();
		}else if(browserName.equalsIgnoreCase("opera"))
		{
			System.setProperty("webdriver.opera.driver", path +"/src/main/resources/drivers/mac/opera/operadriver");
			 driver=new OperaDriver();
		}else if(browserName.equalsIgnoreCase("edge"))
		{
			// Impliment edge code here...
		}else {
			throw new Error("Browser name is not correct, check the browser name once");
		}
		
		
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// 2. Navigate to rediff.com home page
		driver.get(env.getProperty("url"));
		log.debug("Browser opened and navigated to application");
	}
	public void closeBroser() {
		driver.close();
		log.debug("Closed Browser");
	}
	public void captureScreenshot() throws IOException{
		TakesScreenshot capture=(TakesScreenshot)driver;
		File sourceFile=capture.getScreenshotAs(OutputType.FILE);
		long curretTime=System.currentTimeMillis();
		FileUtils.copyFile(sourceFile, new File(path +"/src/main/resources/screenshots/Screenshot"+curretTime+".png"));
		
	}
}
