package com.rediff.common;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ExcelReader extends Common{

	public Object[][] readExcelData() throws EncryptedDocumentException, IOException{
		String path=System.getProperty("user.dir");
		InputStream file=new FileInputStream(path+"/src/main/resources/TestData.xlsx");
		log.debug("Reading the data from excel");
		Workbook workbook=WorkbookFactory.create(file);
		Sheet sheet=workbook.getSheetAt(0);
		
		int lastRowNo=sheet.getLastRowNum();
		log.debug("lastRowNo:"+lastRowNo);
		int totalRowCount=lastRowNo+1;
		
		Row row=sheet.getRow(0);
		
		int lastCellNo=row.getLastCellNum();
		log.debug("lastCellNo:"+lastCellNo);
		
		
		Object o[][]=new Object[totalRowCount][lastCellNo];
		
		//Rows 
		for(int i=0;i<totalRowCount;i++) {
			row=sheet.getRow(i);
			
			//Cells in a Rows
			for(int j=0;j<lastCellNo;j++) {
				Cell cell=row.getCell(j);
				o[i][j]=cell.getStringCellValue();
				//System.out.println(o[i][j]);
			}
			
		}
		
		
		return o;
	}
	
	
	
	public static void main(String[] args) throws EncryptedDocumentException, IOException {
		//ExcelReader excel=new ExcelReader();
		//excel.readExcelData();
		String path=System.getProperty("user.dir");
		System.out.println(path);

	}

}
