package com.rediff.tests.createaccount;

import java.io.IOException;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.rediff.common.Common;

public class CreateAccountTest extends Common {

	@BeforeMethod
	public void openAnyBrowser() {
		openBrowser();
	}

	@Test(priority = 1, enabled = true)
	public void verifyuserNameAvailability() throws InterruptedException, IOException {
		try {
			driver.findElement(By.linkText("Create Account")).click();
			driver.findElement(By.xpath("//td[contains(text(),'Full Name')]/parent::tr/td[3]/input787878"))
					.sendKeys("manidhar");

			driver.findElement(By.xpath("//td[contains(text(),'Choose a Rediffmail ID')]/parent::tr/td[3]/input[1]"))
					.sendKeys("manidhar");
			driver.findElement(By.xpath("//td[contains(text(),'Choose a Rediffmail ID')]/parent::tr/td[3]/input[2]"))
					.click();
			Thread.sleep(1500);
			String actualMessage = driver.findElement(By.xpath("//div[@id='check_availability']/font/b")).getText();
			String expectedMessage = "Sorry, the ID that you are looking for is taken.";

			Assert.assertEquals(actualMessage, expectedMessage);
			log.debug("Verify username availability while creating an account");
		} catch (Exception e) {
			captureScreenshot();
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void closeAllBrowsers() {
		closeBroser();
	}
}
