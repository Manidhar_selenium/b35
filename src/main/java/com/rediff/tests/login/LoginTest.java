package com.rediff.tests.login;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.rediff.common.Common;
import com.rediff.common.ExcelReader;


public class LoginTest extends Common {
	ExcelReader excel = new ExcelReader();

//Comment1
	WebDriver driver = null;
		@BeforeMethod
	public void openAnyBrowser() {
		openBrowser();
	}

	@Test(priority = 1)
	public void verifyPageTitle() throws IOException {
		try {
			SoftAssert softAssertion = new SoftAssert();
			String actualPageTitle = driver.getTitle();
			String expectedPagetitle = "Rediff.com: News | Rediffmail | Stock Quotes | Shopping345345";
			// softAssertion.assertEquals(actualPageTitle, expectedPagetitle,
			// "page titles are not matching, hence failing the test case");

			// Positive testing
			// softAssertion.assertTrue(10>1, "my condtion is failed");
			// softAssertion.assertTrue(actualPageTitle.equals(expectedPagetitle),"Actual
			// and expected titles did not match");
			// Negative testing
			softAssertion.assertFalse(1 > 10);

			softAssertion.assertFalse(actualPageTitle.equals(expectedPagetitle), "negative test failed");

			softAssertion.assertAll();
			log.debug("Verification of page title");
			log.error("This is sample error");
		} catch (Exception e) {
			captureScreenshot();
			e.printStackTrace();
		}

	}

	@Test(priority = 2, dependsOnMethods = "verifyPageTitle", enabled = true, dataProvider = "data")
	public void verifyInvalidLogin(String un, String ps) throws InterruptedException, IOException {
		try {
			driver.findElement(By.linkText("Sign in")).click();
			driver.findElement(By.cssSelector(".loginform input[type=text]")).sendKeys(un);
			driver.findElement(By.id("password")).sendKeys(ps);
			driver.findElement(By.cssSelector(".signinbtn")).click();

			Thread.sleep(1500);
			String actualErrorMessage = driver.findElement(By.id("div_login_error")).getText();
			String expectedErrorMessage = "Wrong username and password combination.";

			Assert.assertEquals(actualErrorMessage, expectedErrorMessage);
			log.debug("Validate login with invalid credentials: " + un + "," + ps);
		} catch (Exception e) {
			captureScreenshot();
			e.printStackTrace();
		}
	}

	@DataProvider
	public Object[][] data() throws EncryptedDocumentException, IOException {

		return excel.readExcelData();
	}

	@AfterMethod
	public void closeAllBrowsers() {
		closeBroser();
	}
}
